const konamiCode = ["KeyW","KeyW","KeyS","KeyS","KeyA","KeyD","KeyA","KeyD","KeyB","KeyA"];
const konamiMobileCode = "WWSSADADBA";

const inputCode = document.querySelector("input");
let sequencePosition = 0;

function codeAccepted() {
  document.documentElement.style.setProperty("--button-color", "blue");
  document.documentElement.style.setProperty("--label-red-color","rgba(0,0,255,0.7)");
  document.querySelector("img").src = "./konami.gif";
}

document.addEventListener("keydown", (e) => {
  if (e.code !== konamiCode[sequencePosition]) {
    sequencePosition = 0;
  } else {
    sequencePosition++;
  }

  if (sequencePosition == konamiCode.length) {
    codeAccepted();
  }
});

inputCode.addEventListener("input", (e) =>{
  if(e.target.value.toUpperCase() === konamiMobileCode){
    codeAccepted();
  }
});