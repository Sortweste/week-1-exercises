const form = document.querySelector("form");

const emailRegExp = new RegExp("^[\\w-\.]+@([\\w-]+\.)+[\\w-]{2,3}$", "i");
const passwordRegExp = new RegExp("^[\\w!@#$]{8,}$");
const phoneRegExp = new RegExp("^([\\d]{8}|([\\d]{4}-[\\d]{4})|(\\+[\\d]{10,11})|(\\(\\+[\\d]{2,3}\\)[\\d]{8}))$");
const websiteRegExp = new RegExp("^(https?:\\/\\/)?(www\.)?([\\w-]+\.)+([\\w-]{2,3})(\\/[\\w-?&]*)*$");

function validateInformation({
  user_name,
  user_lastname,
  user_email,
  user_password,
  user_confirm_password,
  user_phone,
  user_age,
  user_website,
}) {

  let isValidInformation = true;

  if(!user_name){
    assignError("error-name", "Name cannot be blank");
    isValidInformation = false;
  }else{
    clearError("error-name");
  }

  if(!user_lastname){
    assignError("error-lastname", "Lastname cannot be blank");
    isValidInformation = false;
  }else{
    clearError("error-lastname");
  }

  if(!user_password){
    assignError("error-password", "Password cannot be blank");
    isValidInformation = false;
  }else{
    clearError("error-password");
  }

  if(emailRegExp.test(user_email)){
    clearError("error-email");
  }else{
    assignError("error-email", "Invalid email");
    isValidInformation = false;
  }

  if(phoneRegExp.test(user_phone)){
    clearError("error-phone");
  }else{
    assignError("error-phone", "Invalid phone"); 
    isValidInformation = false;
  }
  
  if(websiteRegExp.test(user_website)){
    clearError("error-website");
  }else{
    assignError("error-website", "Invalid website");
    isValidInformation = false;
  }

  if(passwordRegExp.test(user_password)){
    clearError("error-password");
  }else{
    assignError("error-password", "Password must contain 8 characters");
  }

  if(user_password !== user_confirm_password){
    assignError("error-confirm-password", "Password must match");
    isValidInformation = false;
  }else{
    clearError("error-confirm-password");
  }

  if((!user_age) || isNaN(user_age)){
    assignError("error-age", "Invalid age");
    isValidInformation = false;
  }else{
    clearError("error-age");
  }

  return isValidInformation;
}

form.addEventListener("submit", (event) => {
  event.preventDefault();

  let informationSubmitted = {};
  for (const input of form.elements) {
    informationSubmitted[input.name] = input.value;
  }

  if (validateInformation(informationSubmitted)) {
    console.table(informationSubmitted);
  }

});