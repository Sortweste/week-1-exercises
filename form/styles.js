const labels = document.querySelectorAll("label.text-field.empty-field.label-placeholder");

const termsCheckBox = document.getElementById("accepted_terms");
const buttonSignUp = document.querySelector("button");
const inputRange = document.getElementById("experience");

const passwordToggle = document.getElementById("toggle-password");
const passwordConfirmToggle = document.getElementById("toggle-c-password");


labels.forEach((label) => {
  label.previousElementSibling.addEventListener("focusout", (e) => {
    if (label.previousElementSibling.value) {
      label.classList.remove("empty-field");
      label.classList.add("filled-field");
    } else {
      label.classList.remove("filled-field");
      label.classList.add("empty-field");
    }
  });
});

termsCheckBox.addEventListener("click", (e) => {
    if(e.target.checked){
      buttonSignUp.removeAttribute("disabled");
    }else{
      buttonSignUp.setAttribute("disabled", "disabled");
    }
});

inputRange.addEventListener("input",(e)=>{
  switch(e.target.value){
    case "1": 
      e.target.nextElementSibling.textContent="Beginner";
      break;
    case "2": 
      e.target.nextElementSibling.textContent="Intermediate";
      break;
    case "3": 
      e.target.nextElementSibling.textContent="Advanced";
      break;
    case "4": 
      e.target.nextElementSibling.textContent="Expert!";
      break;
  }
});

function togglePassword(passwordId){
  let passwordInput = document.getElementById(passwordId);
  if(passwordInput.type === "password"){
    passwordInput.type = "text";
  }else{
    passwordInput.type = "password";
  }
}

passwordToggle.addEventListener("click", (e) => togglePassword("password"));
passwordConfirmToggle.addEventListener("click", (e) => togglePassword("confirm-password"));

function assignError(errorId, errorMsg){
  let errorSmall = document.getElementById(errorId);
  let iconError = errorSmall.previousElementSibling;
  let labelElement = iconError.previousElementSibling;
  let inputField = labelElement.previousElementSibling;

  errorSmall.classList.add("error-visible");
  errorSmall.textContent = errorMsg;

  iconError.classList.add("error-visible");
  
  labelElement.classList.remove("label-placeholder");
  labelElement.classList.add("error");

  inputField.classList.add("error");

  switch(errorId){
    case "error-password":
        document.getElementById("toggle-password").setAttribute("src", "./eye-error.svg");
      break;
    case "error-confirm-password":
      document.getElementById("toggle-c-password").setAttribute("src", "./eye-error.svg");
      break;
  }

}

function clearError(errorId){
  let errorSmall = document.getElementById(errorId);
  let iconError = errorSmall.previousElementSibling;
  let labelElement = iconError.previousElementSibling;
  let inputField = labelElement.previousElementSibling;

  errorSmall.classList.remove("error-visible");
  errorSmall.textContent = "";

  iconError.classList.remove("error-visible");
  
  labelElement.classList.add("label-placeholder");
  labelElement.classList.remove("error");

  inputField.classList.remove("error");

  switch(errorId){
    case "error-password":
        document.getElementById("toggle-password").setAttribute("src", "./eye-alt.svg");
      break;
    case "error-confirm-password":
      document.getElementById("toggle-c-password").setAttribute("src", "./eye-alt.svg");
      break;
  }

}