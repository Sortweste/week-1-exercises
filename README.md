# Week 1 Assignment
Source code for trainee week 1

The code is deployed in this [website](https://week-1-exercises-git-master.sortweste.vercel.app/main/index.html)

## Create and Validate a Form

The information is validated on submit, these are some aditional restriction:

- Fields cannot be empty.
- Password must have at least 8 characters.
- Valid phone formats:
    - XXXXXXXX
    - XXXX-XXXX
    - +YYYXXXXXXXX
    - (+YYY)XXXXXXXX

## Create our own Konami Code script

The script contains two variants, one for mobile devices and other for desktop.  

### Mobile devices

- The code for mobile devices

        WWSSADADBA

    The code is case insensitive.

You can try this code in an input text field below the gif.

### Desktop

- For desktop, you have no longer the input text field, instead, just press keys in the right sequence and the code will activate.

 - The sequence of keys to press is the same as mobile.
